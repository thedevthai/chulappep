<%@ Page Title="Privacy Policy" Language="VB" AutoEventWireup="true" CodeBehind="PrivacyPolicy.aspx.vb" Inherits=".PrivacyPolicy" %>


   
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-874" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>�к��ҹ�����š�ý֡��Ժѵԧҹ�ԪҪվ ������Ѫ��ʵ�� ����ŧ�ó�����Է�����</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">  

<link rel="stylesheet" type="text/css" href="css/loginstyles.css">
    <link rel="stylesheet" type="text/css" href="css/custyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">

  <link rel="stylesheet" href="components/bootstrap/dist/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/Login.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">  
          
         <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

       <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            function openModalWarningAlert(sender, title, message) {
                $("#spnTitleW").text(title);
                $("#spnMsgW").text(message);
                $('#modal-warningalert').modal('show');
                $('#btnConfirm').attr('onclick', "$('#modal-warningalert').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
                return false;
            }
            function openModalWarningInfo(sender, title, message) {
                $("#spnTitleInfo").text(title);
                $("#spnMsgInfo").text(message);
                $('#modal-warninginfo').modal('show');
                $('#btnConfirm').attr('onclick', "$('#modal-warninginfo').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
                return false;
            }
            function openModalSuccess(sender, title, message) {
                $("#spnTitleS").text(title);
                $("#spnMsgS").text(message);
                $('#modal-success').modal('show');
                $('#btnConfirm').attr('onclick', "$('#modal-success').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
                return false;
            }
        </script>

        <!-- End modal -->
</head>

<body class="hold-transition skin-blue sidebar-mini">
<form id="form2" name="form1"  runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<div class="wrapper">

  <header class="main-header">    
    <a href="#" class="logo">
    <span class="logo-mini"><b>PPEP</b></span>
    <span class="logo-lg"><b>�к��ҹ�����š�ý֡��Ժѵԧҹ�ԪҪվ</b></span>
    </a>
    <nav class="navbar navbar-static-top">     
        <div class="slk-header" >
            <span  class="header-mini">�к��ҹ�����š�ý֡��Ժѵԧҹ�ԪҪվ </span>
           <span  class="header-full">������Ѫ��ʵ�� ����ŧ�ó�����Է�����</span>
        </div>
    </nav>
  </header>



  <div class="contentCPA">
   
<section class="content">

    
                <div class="row"> 
                    <section class="col-lg-12 connectedSortable">
                  
                        <div class="justify-content-center">
                            <div class="col-lg-12">
                                <div class="box box-solid mb-3">
<div class="box-body">
<h3 class="box-title text-center">Ẻ�������âͤ����Թ���������Ǻ�����л����żŢ�������ǹ�ؤ�� </h3>
<div class="scroll-area-lg">

					<div class="txt-prg">
						1. ��������Ҫ�ѭ�ѵԤ�����ͧ��������ǹ�ؤ�� �.�. 2562 ������Ѫ��ʵ�� ����ŧ�ó�����Է����� ����繼��Ǻ�����������ǹ�ؤ�ŷ����˹�ҷ����������㹡�ä�����ͧ��������ǹ�ؤ�ŷ������㹡�ä�ͺ��ͧ���ͤǺ����ͧ����ѷ ��ҹ����ö�ٹ�º�¡�ä�����ͧ��������ǹ�ؤ�Ţͧ����ŧ�ó�����Է����� ����<a href="https://www.chula.ac.th/about/overview/personal-data-protection-policy/" target="_blank">https://www.chula.ac.th/about/overview/personal-data-protection-policy</a>  <br />

                        2. ��������ǹ�ؤ�� ���� ����������ǡѺ�ؤ�ū�觷��������ö�кص�Ǻؤ�Ź���� �����ҷҧ�ç���� �ҧ���� ���������֧�����Ţͧ���֧�������੾��<br />

                        3. ������Ǻ��� �� �����Դ�¢�������ǹ�ؤ�Ţͧ��ҹ����������ѵ�ػ��ʧ��㹡����� �к��ҹ�����š�ý֡��Ժѵԧҹ�ԪҪվ ������Ѫ��ʵ�� ����ŧ�ó�����Է����� ��ҹ�� <br />
4. ��������ǹ�ؤ�Ŵѧ����Ǩж١�����ż��ºؤ�ŷ�����ӹҨ�������Ǣ�ͧ�Ѻ�ѵ�ػ��ʧ�������������ҹ��<br />
5. �������Թ�������������ǹ�ؤ�Ŵѧ����Ǩз���餳����Ѫ��ʵ�� �������ö���Թ��õ���ѵ�ػ��ʧ��ѧ����ǡѺ��ҹ��з�����ҹ�������ö���Ѻ����ª��ҡ��ô��Թ��ôѧ�����<br />
6. ��ҹ���Է�Է��ж͹����Թ���㹡��������Ǻ��� �� �����Դ�¢�������ǹ�ؤ�� �·�ҹ����ö�Դ������˹�ҷ�������ͧ��������ǹ�ؤ�ŷ�� <a href="#" target="_blank"> info@pharm.chula.ac.th</a><br />
7. ������Ѫ��ʵ�� ����ŧ�ó�����Է����� �Ѻ�ͧ����ա�ô��Թ����ѡ�Ҥ�����ʹ��·�����ҵðҹ ��ШѴ������ҵá�ô�ҹ෤�Ԥ��С�èѴ������ͻ�ͧ�ѹ�����Ҷ֧��������ǹ�ؤ�Ţͧ��ҹ���Ԫͺ<br />

                        <br />
                                                 
					 
					</div>  <!-- End txt-prg -->
</div>

    <div class="text-center">
    <asp:CheckBox ID="chkAllow" runat="server" Text="&nbsp;&nbsp;�¢�Ҿ�������ҹ��С�ȩ�Ѻ������ <b>�������Թ���</b> 㹡������������ǹ�ؤ�šѺ ������Ѫ��ʵ�� ���͡�ô��Թ��õ���ѵ�ػ��ʧ���ҧ��" />
        <br />
    </div>
    <br />
     <div class="row justify-content-center text-center">
        <asp:Button ID="cmdSubmit" runat="server" Text="�Թ���" Width="120px" CssClass="btn btn-primary" />
         </div>
      <br />  <br /> 
</div>
                                    
</div>
                                
                            </div>
                        </div>
                    </section>

                </div>
            
                <!-- Modal HTML --> 
               <div id="modal-warninginfo" class="modal fade" role="dialog" data-backdrop="static">
                    <div class="modal-dialog modal-warninginfo">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="icon-box">
                                 <i class="material-icons warning">&#xe002;</i>
                                </div>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body text-center">
                                 <h4><span id="spnTitleInfo"></span></h4>
                                <p><span id="spnMsgInfo"></span></p>
                                <button class="btn" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>  
                <!--- End Modal --->              
            
     
</section>
  
</div>
  <!-- /.content-wrapper -->
  <footer class="main-footer-bottom">
       <div class="pull-right hidden-xs">
       <strong>Copyright &copy; 2018-2020 <a href="http://www.tansomros.com">tansomros</a>.</strong> All rights
    reserved.
       <br />     <b>Version</b> <asp:Label ID="lblVersion" runat="server" Text="2.0.0"></asp:Label>&nbsp;&nbsp;<b>Release</b> 2018.10.01
    </div>
       

      <b>˹��½֡��Ժѵԧҹ�ԪҪվ ������Ѫ��ʵ�� ����ŧ�ó�����Է�����</b>
        <br />254 ������� ࢵ�����ѹ ��ا෾��ҹ�� 10330
          ���Ѿ��  02-218-8450-1 ����� 02-218-8450
    
  </footer>
  
</div>
<!-- ./wrapper --> 
</form>
    
<script src="components/jquery/dist/jquery.min.js"></script>
<script src="components/jquery-ui/jquery-ui.min.js"></script>
<script src="components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="dist/js/adminlte.min.js"></script>

     
</body>
</html>
  