﻿
Public Class ucMenu
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '        nbMain.AllowExpanding = True
        '        nbMain.AllowSelectItem = True
        '        nbMain.AutoCollapse = True
        '        nbMain.EnableHotTrack = True
        '        nbMain.EnableAnimation = True


        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupBiography")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupMain")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupStudentSelection")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupSupervision")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupStudent")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupTeacher")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupSubject")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupLocation")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupMainYearData")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupPicking")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupSelection")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupReport")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupConfiguration")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupUserManage")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupNews")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupDocument")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupTimePhase")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupEvaluation")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupAssessment")).Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupAssessmentResult")).Visible = False
        '        'nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupRecomend")).Visible = False



        '        nbMain.Items.FindByName("mnuPrintBio").NavigateUrl = "ReportViewerStudentBio.aspx?id=" & Request.Cookies("ProfileID").Value

        '        nbMain.Items.FindByName("mnuSpvManage").Visible = False
        '        nbMain.Items.FindByName("mnuSendJob").Visible = False
        '        nbMain.Items.FindByName("mnuRPTRequirementStd").Visible = False
        '        nbMain.Items.FindByName("mnuRPTStudentRegister").Visible = False
        '        nbMain.Items.FindByName("mnuRPTStudentManage").Visible = False
        '        nbMain.Items.FindByName("mnuRPTStudentList_Doc").Visible = False
        '        nbMain.Items.FindByName("mnuRPTStudentTimePhase").Visible = False
        '        nbMain.Items.FindByName("mnuRPTCertificate").Visible = False
        '        'nbMain.Items.FindByName("mnuRPTAssessmentScore").Visible = False
        '        nbMain.Items.FindByName("mnuRPTStudentCourse").Visible = False
        '        nbMain.Items.FindByName("mnuRPTSummary4Payment").Visible = False

        '        nbMain.Items.FindByName("mnuGradeConfig").Visible = False
        '        nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupSupervision")).Visible = False
        '        nbMain.Items.FindByName("mnuSpvLocation").Visible = False



        '        nbMain.Items.FindByName("mnuAssessmentStudent").Visible = False
        '        'nbMain.Items.FindByName("mnuAssessmentReview").Visible = False
        '        'nbMain.Items.FindByName("mnuAssessmentStatus").Visible = False
        '        'nbMain.Items.FindByName("mnuAssessmentGradeResult").Visible = False

        '        nbMain.Items.FindByName("mnuEvaGroup").Visible = False
        '        nbMain.Items.FindByName("mnuEvaTopic").Visible = False
        '        nbMain.Items.FindByName("mnuEvaSubject").Visible = False
        '        nbMain.Items.FindByName("mnuAssessmentSubject").Visible = False

        '        'nbMain.Items.FindByName("mnuRecommendManage").Visible = False



        '#Region "Student"
        '        If Request.Cookies("ROLE_STD").Value = True Then
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupBiography")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupStudentSelection")).Visible = True

        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupReport")).Visible = True
        '            'nbMain.Items.Item(34).Visible = True
        '            'nbMain.Items.Item(35).Visible = False
        '            nbMain.Items.FindByName("mnuRPTRequirementStd").Visible = True
        '            nbMain.Items.FindByName("mnuRPTStudentRegister").Visible = True

        '            'nbMain.Items.FindByName("mnuRPTStudentManage").Visible = False
        '            'nbMain.Items.FindByName("mnuRPTStudentList_Doc").Visible = False
        '            'nbMain.Items.FindByName("mnuRPTStudentTimePhase").Visible = False
        '            'nbMain.Items.FindByName("mnuRPTAssessment").Visible = False
        '            'nbMain.Items.FindByName("mnuRPTStudentCourse").Visible = False
        '            'nbMain.Items.FindByName("mnuRPTSummary4Payment").Visible = False
        '            nbMain.Items.FindByName("mnuRPTCertificate").Visible = False
        '            'nbMain.Items.FindByName("mnuRPTAssessmentScore").Visible = False
        '            nbMain.Items.FindByName("mnuRPTStudentManage").Visible = False
        '            nbMain.Items.FindByName("mnuRPTStudentCourse").Visible = False
        '            nbMain.Items.FindByName("mnuRPTStudentList_Doc").Visible = False
        '            nbMain.Items.FindByName("mnuRPTSummary4Payment").Visible = False
        '            nbMain.Items.FindByName("mnuRPTStudentTimePhase").Visible = False
        '            nbMain.Items.FindByName("mnuStudentCountByPhase").Visible = False
        '            nbMain.Items.FindByName("mnuPaymentBySubject").Visible = False
        '        End If
        '#End Region
        '#Region "Faculty Member"
        '        If (Request.Cookies("ROLE_TCH").Value = True) And (Request.Cookies("ROLE_ADV").Value = False) Then

        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupStudent")).Visible = True
        '            nbMain.Items.FindByName("mnuStudentBioPrint").Visible = True
        '            nbMain.Items.FindByName("mnuStudentData").Visible = False

        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupSupervision")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupEvaluation")).Visible = False
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupAssessment")).Visible = False
        '            nbMain.Items.FindByName("mnuAssessmentStudent").Visible = False

        '            'nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupAssessmentResult")).Visible = True

        '            'nbMain.Items.FindByName("mnuAssessmentReview").Visible = False
        '            'nbMain.Items.FindByName("mnuAssessmentStatus").Visible = False
        '            'nbMain.Items.FindByName("mnuAssessmentGradeResult").Visible = True

        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupReport")).Visible = True


        '            nbMain.Items.FindByName("mnuRPTRequirementStd").Visible = True
        '            nbMain.Items.FindByName("mnuRPTStudentRegister").Visible = True
        '            nbMain.Items.FindByName("mnuRPTStudentManage").Visible = True
        '            nbMain.Items.FindByName("mnuRPTStudentList_Doc").Visible = False
        '            nbMain.Items.FindByName("mnuRPTStudentTimePhase").Visible = True
        '            nbMain.Items.FindByName("mnuRPTCertificate").Visible = True
        '            'nbMain.Items.FindByName("mnuRPTAssessmentScore").Visible = True
        '            nbMain.Items.FindByName("mnuRPTStudentCourse").Visible = True
        '            nbMain.Items.FindByName("mnuRPTSummary4Payment").Visible = False
        '            nbMain.Items.FindByName("mnuPaymentBySubject").Visible = False


        '        End If
        '#End Region

        '#Region "Advisor"
        '        If Request.Cookies("ROLE_ADV").Value = True Then

        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupStudent")).Visible = True
        '            nbMain.Items.FindByName("mnuStudentBioPrint").Visible = True
        '            nbMain.Items.FindByName("mnuStudentData").Visible = False

        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupSupervision")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupEvaluation")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupAssessment")).Visible = True

        '            nbMain.Items.FindByName("mnuAssessmentStudent").Visible = True

        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupSupervision")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupAssessmentResult")).Visible = True

        '            nbMain.Items.FindByName("mnuAssessmentReview").Visible = False
        '            nbMain.Items.FindByName("mnuAssessmentStatus").Visible = False
        '            nbMain.Items.FindByName("mnuAssessmentGradeResult").Visible = True

        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupReport")).Visible = True

        '            nbMain.Items.FindByName("mnuRPTRequirementStd").Visible = True
        '            nbMain.Items.FindByName("mnuRPTStudentRegister").Visible = True
        '            nbMain.Items.FindByName("mnuRPTStudentManage").Visible = True
        '            nbMain.Items.FindByName("mnuRPTStudentList_Doc").Visible = False
        '            nbMain.Items.FindByName("mnuRPTStudentTimePhase").Visible = True
        '            nbMain.Items.FindByName("mnuRPTCertificate").Visible = True
        '            'nbMain.Items.FindByName("mnuRPTAssessmentScore").Visible = True
        '            nbMain.Items.FindByName("mnuRPTStudentCourse").Visible = True
        '            nbMain.Items.FindByName("mnuRPTSummary4Payment").Visible = False
        '            nbMain.Items.FindByName("mnuPaymentBySubject").Visible = False


        '        End If
        '#End Region

        '#Region "Preceptor"
        '        If (Request.Cookies("ROLE_PCT").Value = True) Then
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupMain")).Visible = True
        '            'nbMain.Groups(nbMain.Groups.IndexOfName("mnuAssessment")).Visible = True
        '            'nbMain.Items.FindByName("mnuAssessmentStudent").Visible = True 
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupRecomend")).Visible = False
        '        End If
        '#End Region
        '#Region "Admin"
        '        If Request.Cookies("ROLE_ADM").Value = True Then
        '            nbMain.Items.FindByName("mnuStudentData").Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupEvaluation")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupAssessment")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupAssessmentResult")).Visible = True

        '            nbMain.Items.FindByName("mnuAssessmentReview").Visible = True
        '            nbMain.Items.FindByName("mnuAssessmentStatus").Visible = True
        '            nbMain.Items.FindByName("mnuAssessmentGradeResult").Visible = True

        '            nbMain.Items.FindByName("mnuGradeConfig").Visible = True
        '            nbMain.Items.FindByName("mnuEvaGroup").Visible = True
        '            nbMain.Items.FindByName("mnuEvaTopic").Visible = True
        '            nbMain.Items.FindByName("mnuEvaSubject").Visible = True
        '            nbMain.Items.FindByName("mnuAssessmentSubject").Visible = True

        '            nbMain.Items.FindByName("mnuSpvManage").Visible = True
        '            nbMain.Items.FindByName("mnuSendJob").Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupMain")).Visible = False
        '            'nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupSupervision")).Visible = False
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupStudent")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupTeacher")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupSubject")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupLocation")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupMainYearData")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupPicking")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupSelection")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupReport")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupConfiguration")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupUserManage")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupNews")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupDocument")).Visible = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupTimePhase")).Visible = True

        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupSupervision")).Visible = True
        '            nbMain.Items.FindByName("mnuSpvLocation").Visible = True

        '            nbMain.Items.FindByName("mnuRPTRequirementStd").Visible = True
        '            nbMain.Items.FindByName("mnuRPTStudentRegister").Visible = True
        '            nbMain.Items.FindByName("mnuRPTStudentManage").Visible = True
        '            'nbMain.Items.FindByName("mnuRPTStudentList_Doc").Visible = True
        '            nbMain.Items.FindByName("mnuRPTStudentTimePhase").Visible = True
        '            'nbMain.Items.FindByName("mnuRPTAssessmentScore").Visible = True
        '            nbMain.Items.FindByName("mnuRPTCertificate").Visible = True
        '            nbMain.Items.FindByName("mnuRPTStudentCourse").Visible = True
        '            nbMain.Items.FindByName("mnuRPTSummary4Payment").Visible = False

        '            'nbMain.Items.FindByName("mnuRecommendManage").Visible = True
        '        End If
        '#End Region
        '#Region "Super Admin"
        '        If Request.Cookies("ROLE_SPA").Value = True Then
        '            nbMain.Items.FindByName("mnuDocumentMaster").Visible = True
        '            'nbMain.Items.FindByName("mnuRecommendManage").Visible = True
        '        Else
        '            nbMain.Items.FindByName("mnuDocumentMaster").Visible = False
        '        End If
        '#End Region


        '        nbMain.Groups.CollapseAll()

        '        If Request("p") = "a" Then
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupEvaluation")).Expanded = True
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupAssessment")).Expanded = True
        '            nbMain.Items.FindByName("mnuAssessmentStudent").Selected = True
        '        End If
        '        If Request("p") = "ad" Then
        '            nbMain.Groups(nbMain.Groups.IndexOfName("nbGroupAssessmentResult")).Expanded = True
        '            nbMain.Items.FindByName("mnuAssessmentReview").Selected = True
        '        End If

    End Sub

End Class
