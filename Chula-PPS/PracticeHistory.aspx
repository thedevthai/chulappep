﻿
<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PracticeHistory.aspx.vb" Inherits=".PracticeHistory" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <section class="content-header">
      <h1>ประวัติการฝึกปฏิบัติงานวิชาชีพ</h1>     
    </section>
<section class="content">
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-registered"></i>

              <h3 class="box-title">ประวัติการฝึกปฏิบัติงานวิชาชีพทั้งหมด <asp:Label ID="lblCount" runat="server"></asp:Label>&nbsp;รายการ
                </h3>             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
             
              </div>                                 
            </div>
            <div class="box-body">          


<table width="100%" border="0" cellspacing="2" cellpadding="0">
  <tr>
    <td align="center">
             <asp:Label ID="lblNot" runat="server" CssClass="validateAlert" 
                 Text="ยังไม่มีประวัติการฝึกปฏิบัติงานวิชาชีพ" Width="99%"></asp:Label>
          </td>
  </tr>
  <tr>
    <td>
           <table id="tbdata" class="table table-bordered table-striped">
                <thead>
                <tr> 
                    <th>รหัสนักศึกษา</th>
                    <th>ชื่อนักศึกษา</th>
                    <th>สาขา</th>
                    <th>ชั้นปีที่</th>                     
                    <th>Active</th>
                    <th class="sorting_asc_disabled">ดูประวัติ</th> 
                    <th class="sorting_asc_disabled">ประวัติฝึกฯ</th> 
                    <th class="sorting_asc_disabled">แก้ไข</th> 
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtHis.Rows %>
                <tr>              
                  <td class="text-center"><% =String.Concat(row("Student_Code")) %></td>
                  <td><% =String.Concat(row("StudentName")) %>    </td>
                  <td class="text-center"><% =String.Concat(row("MajorName")) %></td>
                  <td class="text-center"><% =String.Concat(row("ClassName")) %></td>              
                  <td class="text-center"><% =IIf(String.Concat(row("StatusFlag")) = "A", "<img src='images/icon-ok.png'>", "") %> </td>
                  <td class="text-center"> 
                      <a class="editemp" href="Student_Bio?ActionType=std&std=<% =String.Concat(row("Student_Code")) %>" target="_blank"><img src="images/view.png" data-toggle="tooltip" data-placement="top" data-original-title="ดูประวัติ"  /></a>
</td>
                  <td class="text-center"> 
                      <a class="editemp" href="PracticeHistory?ActionType=std&ItemType=pthistory&std=<% =String.Concat(row("Student_Code")) %>" target="_blank"><img src="images/learn.png" data-toggle="tooltip" data-placement="top" data-original-title="ประวัติฝึกฯ" /></a>
</td>
                  <td class="text-center"> 
                       <a class="editemp" href="Student_Reg?ActionType=std&ItemType=std&stdid=<% =String.Concat(row("Student_Code")) %>" target="_blank"><img src="images/icon-edit.png" data-toggle="tooltip" data-placement="top" data-original-title="แก้ไข" /></a>                  
                      
                    </td>    
                </tr>
            <%  Next %>
                </tbody>               
              </table>  

         <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" CssClass="txtcontent" 
                             Font-Bold="False" Width="100%" AutoGenerateColumns="False">
                        <RowStyle BackColor="#F7F7F7" />
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Left" 
                                                            />                     
                        <Columns>
                            <asp:BoundField DataField="EduYear" HeaderText="ปีการศึกษา" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SubjectCode" HeaderText="รหัสวิชา" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="รายวิชา" DataField="SubjectName" />
                            <asp:BoundField HeaderText="แหล่งฝึก" DataField="LocationName" />
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView>  </td>
  </tr>
</table>
 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>
</asp:Content>
